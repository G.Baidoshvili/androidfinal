package com.example.gigaapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()

    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
        updateUI(currentUser)
    }

    private fun updateUI(currentUser: FirebaseUser?){

    }

    private fun init()
    {
        auth = FirebaseAuth.getInstance();
        SignUpButton.setOnClickListener{
            OpenRegisterActivity()
        }

        LogInButton.setOnClickListener{
            LogInUser()
        }
    }

    private fun OpenRegisterActivity()
    {
        val intent = Intent(this, RegisterActivity::class.java)
        startActivity(intent)
        finish()
    }


    private fun LogInUser()
    {
        if (EmailEditText.text.toString().isEmpty()) {
            EmailEditText.error = "Please enter email"
            EmailEditText.requestFocus()
            return
        }


        if (!Patterns.EMAIL_ADDRESS.matcher(EmailEditText.text.toString()).matches()) {
            EmailEditText.error = "Please enter valid email"
            EmailEditText.requestFocus()
            return
        }

        if (PasswordEditText.text.toString().isEmpty()) {
            PasswordEditText.error = "Please enter password"
            PasswordEditText.requestFocus()
            return
        }


        auth.signInWithEmailAndPassword(EmailEditText.text.toString(), PasswordEditText.text.toString())
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    val intent = Intent(this, ListActivity::class.java)
                    Toast.makeText(this,"Authentication successful. Welcome!",Toast.LENGTH_SHORT).show()
                    startActivity(intent)
                    finish()
                } else {
                    Toast.makeText(this,"Authentication failed!!!",Toast.LENGTH_SHORT).show()
                }
            }
    }


}
