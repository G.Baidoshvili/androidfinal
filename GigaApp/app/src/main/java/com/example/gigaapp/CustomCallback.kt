package com.example.gigaapp

interface CustomCallback {
    public fun onSuccess (result:String, activity: ListActivity){}
    public fun onFailure(error:String, activity: ListActivity){}
}