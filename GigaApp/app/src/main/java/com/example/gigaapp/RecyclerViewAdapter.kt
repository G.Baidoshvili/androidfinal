package com.example.gigaapp

import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.recycler_view_item.view.*

class RecyclerViewAdapter(private val items:ArrayList<TrackModel>, private val activity: ListActivity) : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.recycler_view_item,
            parent,
            false))


    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    {


        private lateinit var model:TrackModel
        fun onBind()
        {
            model = items[adapterPosition]
            Glide.with(activity).load(model.image).into(itemView.imageView)
            itemView.titleTextView.text = model.trackName
            itemView.descriptionTextView.text =
                "artist: ${model.artistName},\n" +
                        " album: ${model.collectionName} \n" +
                        "country of origin: ${model.country} \n" +
                        "genre: ${model.genre} \n" +
                        "number of tracks on the album: ${model.trackCount} \n" +
                        "track number: ${model.trackNumber} \n" +
                        "length: ${model.trackTimeMillis/(1000*60)}:${model.trackTimeMillis/1000%60}"
        }
    }
}