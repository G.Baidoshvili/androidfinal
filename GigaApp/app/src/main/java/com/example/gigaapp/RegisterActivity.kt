package com.example.gigaapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.EmailEditText
import kotlinx.android.synthetic.main.activity_main.PasswordEditText
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        init()
    }

    private fun init()
    {
        auth = FirebaseAuth.getInstance();
        RegisterButton.setOnClickListener{
            RegisterUser()
        }

        BackButton.setOnClickListener{
            Back()
        }
    }

    private fun RegisterUser()
    {
        if (EmailEditText.text.toString().isEmpty()) {
            EmailEditText.error = "Please enter email"
            EmailEditText.requestFocus()
            return
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(EmailEditText.text.toString()).matches()) {
            EmailEditText.error = "Please enter valid email"
            EmailEditText.requestFocus()
            return
        }

        if (PasswordEditText.text.toString().isEmpty()) {
            PasswordEditText.error = "Please enter password"
            PasswordEditText.requestFocus()
            return
        }

        if (ConfirmPasswordEditText.text.toString().isEmpty()) {
            ConfirmPasswordEditText.error = "Please enter password"
            ConfirmPasswordEditText.requestFocus()
            return
        }

        if(!ConfirmPasswordEditText.text.toString().equals(PasswordEditText.text.toString()))
        {
            ConfirmPasswordEditText.error = "Passwords do not match"
            ConfirmPasswordEditText.requestFocus()
            return
        }

        auth.createUserWithEmailAndPassword(EmailEditText.text.toString(), PasswordEditText.text.toString())
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    val intent = Intent(this, ListActivity::class.java)
                    Toast.makeText(this,"Registration successful. Welcome!",Toast.LENGTH_SHORT).show()
                    startActivity(intent)
                    finish()
                } else {
                    Toast.makeText(this,"This email is taken! Registration failed!",Toast.LENGTH_SHORT).show()
                }
            }
    }

    private fun Back()
    {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}
