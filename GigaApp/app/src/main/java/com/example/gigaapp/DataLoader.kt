package com.example.gigaapp

import android.util.Log.d
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path


object DataLoader {
    private var retrofit = Retrofit.Builder()
        .addConverterFactory(ScalarsConverterFactory.create())
        .baseUrl("https://giga-app-49f5c.firebaseio.com/")
        .build()

    private var service = retrofit.create(ApiRetrofit::class.java)

    fun getRequest(path:String, customCallback: CustomCallback, activity: ListActivity){
        val call = service.getRequest(path)
        call.enqueue(object:Callback<String>{
            override fun onFailure(call: Call<String>, t: Throwable) {
                customCallback.onFailure(t.message.toString(),activity)
            }

            override fun onResponse(call: Call<String>, response: Response<String>) {
                customCallback.onSuccess(response.body().toString(),activity)
            }

        })
    }
}

interface ApiRetrofit {
    @GET("{path}")
    fun getRequest(@Path("path") path: String): Call<String>
}