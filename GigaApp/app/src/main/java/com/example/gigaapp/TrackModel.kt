package com.example.gigaapp

import com.google.gson.annotations.SerializedName


class TrackModel {

    var trackName = ""
    var artistName = ""
    @SerializedName("artworkUrl100")
    var image = ""
    var collectionName = ""
    var country = ""
    @SerializedName("primaryGenreName")
    var genre = ""
    var trackCount = 0
    var trackNumber = 0
    var trackTimeMillis = 0




}

