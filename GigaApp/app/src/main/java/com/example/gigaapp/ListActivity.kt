package com.example.gigaapp

import android.os.Bundle
import android.util.Log.d
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_list.*

class ListActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)
        init()
    }

    private fun init()
    {
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = RecyclerViewAdapter(ArrayList<TrackModel>(),this)
        GetItems()
    }

    private fun GetItems()
    {
        DataLoader.getRequest("results.json",object:CustomCallback{
            override fun onSuccess(result: String, activity: ListActivity) {

                val models = Gson().fromJson(
                    result,
                    Array<TrackModel>::class.java
                )

                recyclerView.adapter = RecyclerViewAdapter(models.toCollection(ArrayList()),activity)

            }
        },this)

    }
}
